$(document).ready(function() {
  $('table').on('click', 'a#elimina', function(){
    var nome = $(this).closest("tr").find('td:eq(0)').text();
    $(this).closest('tr').remove();
    $.ajax({
      type: 'post',
      url: 'proc/dropdish.php',
      data: { nome : nome}
    });
  });
});
