
$("document").ready(function() {
  var slideIndex = 0;
  var numSlides = $(".mySlides").length;

  $(".mySlides").hide();
  $(".mySlides:first").show();
  showSlides();

  function showSlides() {

    $(".mySlides").hide();

    slideIndex++;
    if (slideIndex > numSlides) {
      slideIndex = 1;
    }
    $(".mySlides:nth-of-type(" + slideIndex + ")").show();
    setTimeout(showSlides, 4000);

  }
});
