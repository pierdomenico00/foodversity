$(document).ready(function() {
  var nome = $(this).find("h2.ristoranti-title").text();
  $.ajax({
    type: 'post',
    url: 'proc/getcategories.php',
    data: { nome : nome },
    success: function(result) {
      var c = jQuery.parseJSON(result);
      var index = c.length;
      for (var i = 0; i < index; i++) {
        switch(c[i].idcat) {
          	case '10':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="antipasti">
                    <img src="sources/ham-leg.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="10">Antipasti</h4></div>
              </div>`);
          		break;
          	case '11':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="primi">
                    <img src="sources/spaguetti.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="11">Primi piatti</h4></div>
              </div>`);
          		break;
          	case '12':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="secondi">
                    <img src="sources/meat.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="12">Secondi piatti</h4></div>
              </div>`);
          		break;
          	case '13':
              $('#categorie').append(`<div class="col-md-3" id="zuppe">
                  <div class="square">
                    <img src="sources/soup.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="13">Zuppe</h4></div>
              </div>`);
          		break;
            case '4':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id ="panini">
                    <img src="sources/burger.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="4">Panini</h4></div>
              </div>`);
          		break;
          	case '3':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="snack">
                    <img src="sources/french-fries.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="3">Snack</h4></div>
              </div>`);
          		break;
          	case '2':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="pizze">
                    <img src="sources/pizza.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="2">Pizze</h4></div>
              </div>`);
          		break;
          	case '5':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="piadine">
                    <img src="sources/crepe.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="5">Piadine</h4></div>
              </div>`);
          		break;
            case '6':
              $('#categorie').append(`<div class="col-md-3">
                    <div class="square" id="insalate">
                      <img src="sources/salad.png" class="img img-responsive img-invert"/>
                    </div>
                  <div class="caption center-block text-font"><h4 id="6">Insalate</h4></div>
                </div>`);
          		break;
            case '7':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="dolci">
                    <img src="sources/piece-of-cake.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="7">Dolci</h4></div>
              </div>`);
          		break;
          	case '8':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="bibite">
                    <img src="sources/soda.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="8">Bibite</h4></div>
              </div>`);
          		break;
          	case '9':
              $('#categorie').append(`<div class="col-md-3">
                  <div class="square" id="altro">
                    <img src="sources/serving-dish.png" class="img img-responsive img-invert"/>
                  </div>
                <div class="caption center-block text-font"><h4 id="9">Altro</h4></div>
              </div>`);
          		break;
          	default:
          		alert('Errore nel caricamento delle categorie!');
        }
      }
    }
  });
});
