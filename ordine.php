<?php
session_start();
$_SESSION["numero"] = $_GET["numero"];
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Dettagli ordine</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="supplorders.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
          <a href="dashboardcliente.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
      </ul>
      </div>
    </nav>
    <div id="prodotto" class="container">
    </div>
    <div class="clear"></div>
  </body>
