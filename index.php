<?php
  session_start();
  if(isset($_COOKIE["remember_me"])){
    $_SESSION["idcliente"] = $_COOKIE["remember_me"];
  }
  if(isset($_COOKIE["remember_suppl"])){
    $_SESSION["idsuppl"] = $_COOKIE["remember_suppl"];
  }
  if(isset($_SESSION["idcliente"])){
      header("location:dashboardcliente.php");
  }
  if(isset($_SESSION["idsuppl"])){
    header("location:dashboardfornitore.php");
  }
 ?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>FoodVersity homepage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="slideshow.js" type="text/javascript"></script>
  </head>
  <body id="homepage">
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">FoodVersity</a>
          <a href="index.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right" >
          <button type="button" class="btn btn-basic button-noBorder" onclick="location='login.php'">Accedi</button>
          <button type="button" class="btn btn-default button-squared" onclick="location='signup.php'">Registrati</button>
        </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="well">
            <h2>Food delivery per studenti universitari, creato da studenti universitari.</h2>
            <p class="homepage-text" style="text-align:justify">
              <span style="font-weight:bold">Rapido</span> - iscriviti come utente o come fornitore in meno di due minuti. <br/>
              <span style="font-weight:bold">Semplice</span> - piattaforma interattiva, facile da usare e intuitiva. <br/>
              <span style="font-weight:bold">Comodo</span> - i piatti dei tuoi ristoranti preferiti direttamente al campus universitario. <br/> <br/>
              <span style="font-style:italic"> "Feed your mind"</span>
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="slideshow-container">
            <div class="mySlides fade">
              <div class="numbertext">Premium partner</div>
              <img src="sources/img8.jpg" height="400" width="400">
            </div>
            <div class="mySlides fade">
              <div class="numbertext">Premium partner</div>
              <img src="sources/img2.jpg" height="400" width="400">
            </div>
            <div class="mySlides fade">
              <div class="numbertext">Premium partner</div>
              <img src="sources/img3.jpg" height="400" width="400">
            </div>
            <div class="mySlides fade">
              <div class="numbertext">Premium partner</div>
              <img src="sources/img4.jpg" height="400" width="400">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="offset-md-11 col-md-1 bottom-left-btn">
          <button type="button" class="btn btn-default btn-sm" onclick="location='loginfornitore.php'">Area fonitori</button>
        </div>
      </div>
    </div>
  </body>
</html>
