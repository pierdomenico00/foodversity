$(document).on("click", "#upload", function() {
  var file_data = $("#image").prop("files")[0];
  var form_data = new FormData();
  form_data.append("image", file_data)
  $.ajax({
    url: "proc/image.php",
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    success: function(result){
      document.getElementById("statusphoto").innerHTML = result;
    }
  })
})
