$(function () {
  $('#form').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: 'proc/add_dish.php',
      data: $('form').serialize(),
      success: function(result) {
        alert(result);
        $('form').find("input[type=text], textarea").val("");
        $('form').find("input[type=number], textarea").val("");
      }
    });
  });
});
