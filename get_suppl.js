$(document).ready( function () {
  $.ajax({
   url: "./proc/get_suppl.php"
  }).done(function(result) {
     var c = jQuery.parseJSON(result);
     var index = c.length;
     for (var i = 0; i < index; i++) {
       $('#rest').append(`<div class="col-md-6">
             <div class="media">
               <div class="media-left">
                 <img src="./supplimg/`+c[i].idfornitore+`.jpg" class="media-object">
               </div>
               <div class="media-body">
                 <h4 id="nome" style="text-align: left" class="media-heading">`+c[i].nome+`</h4>
                 <p class="media-body-text">Indirizzo: `+c[i].ind_via+`, `+c[i].ind_civico+` - `+c[i].ind_citta+`</p>
                 <p style="word-break:break-all">`+c[i].email+`</p>
               </div>
             </div>
       </div>`);
     }
   });
});
