<?php
session_start();
  if (isset($_SESSION["idcliente"]) || isset($_SESSION["idsuppl"]))
   {
      header("location:dashboardcliente.php");
   }
?>
<!DOCTYPE html>
<html lang="it">

<head>
  <title>FoodVersity registration page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="user_signup.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">FoodVersity</a>
        <a href="index.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
        </a>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Registrazione</h4></div>
            <div class="panel-body animated-form">
              <form name="form" method="post">
                <div class="form-group">
                  <label class="form-label" for="nome">Nome:</label>
                  <input type="text" class="form-control" id="nome" placeholder="Inserisci il tuo nome" name="nome" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="cognome">Cognome:</label>
                  <input type="text" class="form-control" id="cognome" placeholder="Inserisci il tuo cognome" name="cognome" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="matricola">Matricola:</label>
                  <input type="number" class="form-control" id="matricola" placeholder="0000000000" name="matricola" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="email">Email:</label>
                  <input type="email" class="form-control" id="email" placeholder="prova@prova.it" name="email" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="password">Password:</label>
                  <input type="password" class="form-control" id="password" placeholder="********" name="password" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="confirm-password">Conferma password:</label>
                  <input type="password" class="form-control" id="passwordconf" placeholder="********" name="passwordconf" required>
                </div>
                <span style="color : white" id="statusmsg"></span><br>
                <button type="submit" class="btn btn-default" value="Submit">Invia</button>
              </div>
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </body>
