<?php
session_start();
require_once "proc/connect.php";
if(!isset($_SESSION["idsuppl"])){
  header("location:index.php");
}
$id=$_SESSION["idsuppl"];
$sth = mysqli_query($link, "SELECT nome, ingredienti, costo FROM piatto WHERE idfornitore = '$id'");
$rows = array();
while($r = mysqli_fetch_assoc($sth)) {
    $rows[] = $r;
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Ristorante</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        var ar = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($rows);?>));
        var index = ar.length;
        for (var i = 0; i < index; i++) {
            $('#tabella').append(
              `<tr>
                <td>`+ar[i].nome+`</td>
                <td>`+ar[i].ingredienti+`</td>
                <td>`+ar[i].costo+` €</td>
                <td style="text-align:center"><a href="modificapiatto.php?nome=`+ar[i].nome+`" style="text-align:center">Modifica</a></td>
                <td style="text-align:center"><a id="elimina" href="#" style="text-align:center">Elimina</a></td>
                </tr>`);
        }
      });
    </script>
    <script src="dropdish.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
          <a href="dashboardfornitore.php">
            <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
        </ul>
      </div>
    </nav>
    <div class="container"  style="margin-bottom:30px">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
          <table style="background:white;" class="resp-table">
            <thead>
              <tr>
                <th style="width:20%; text-align:center">Nome piatto</th>
                <th style="width:35%; text-align:center">Ingredienti</th>
                <th style="width:15%; text-align:center">Prezzo</th>
                <th style="width:15%"></th>
                <th style="width:15%"></th>
              </tr>
            </thead>
            <tbody id="tabella">
            </tbody>
          </table>
        </div>
      </div>
    </div>
</body>
</html>
