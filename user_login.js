$(function () {
  $('form').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: 'proc/usercheck.php',
      data: $('form').serialize(),
      success: function(result) {
          if(result == "Login riuscito") {
            window.location.href='dashboardcliente.php';
          } else {
            document.getElementById("statusmsg").innerHTML = result;
          }
      }
    });
  });
});
