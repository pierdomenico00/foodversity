$(document).ready(function() {
  $('div.jumbotron').on('click', 'button.delate-button', function(){
    var nome = $(this).closest("div.row").find('h2').text();
    var prezzo = $(this).closest('div.jumbotron').find('#price').text();
    var quantita = $(this).closest('div.jumbotron').find('span#quantita').text();
    $.ajax({
      type: 'post',
      url: 'proc/cartop.php?toremove='+nome+'',
      success: function() {
        prezzo = parseFloat(prezzo.substr(0,prezzo.indexOf(' '))).toFixed(2);
        prezzo = prezzo * quantita;
        totale = $('#totale').text();
        totale = parseFloat(totale.substr(8,totale.indexOf(' '))).toFixed(2);
        totale = totale - prezzo;
        totale = parseFloat(totale).toFixed(2);
        $('#totale').text('Totale: '+totale+' €');
        $('div.jumbotron h2:contains('+nome+')').closest('#dish').remove();
      }
    });
  });
});
