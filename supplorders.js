$(document).ready(function getPendingOrder() {
  $.ajax({
        url: "proc/supplorders.php",
        type: "get",
  }).done(function(result) {
          var ar = jQuery.parseJSON(result);
          var index = ar.length;
          var total = 0;
          for (var i = 0; i < index; i++) {
            $('#prodotto').append(`<div id="dish" class="jumbotron">
                      <div class="row">
                        <div class="col-md-11">
                          <h2 class="dish-name">`+ar[i].nome+`</h2>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-7">
                          <p>`+ar[i].ingredienti+`</p>
                        </div>
                        <div class="col-md-3">
                          <p>Quantità : `+ar[i].quantita+`</p>
                        </div>
                        <div class="col-md-2">
                          <div class="price"><span id="price" class="input-group-addon">Per pezzo: €`+ar[i].costo+`</span></div>
                        </div>
                      </div>
                  </div>`);
                  total = total + Number(ar[i].costo)*Number(ar[i].quantita);
                }
                total = parseFloat(total).toFixed(2);
                $('#totale').text('Totale: '+total+' €');
          });
});
