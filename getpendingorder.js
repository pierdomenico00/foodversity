$(document).ready(function getPendingOrder() {
  $.ajax({
        url: "proc/getpendingorder.php",
        type: "get",
  }).done(function(result) {
          $('#ordinipendenti').empty();
          var ar = jQuery.parseJSON(result);
          var index = ar.length;
          for (var i = 0; i < index; i++) {
              $('#ordinipendenti').append(
                `<tr>
                  <td class="td-word-break">`+ar[i].numordine+`</td>
                  <td>Ingresso università</td>
                  <td>`+ar[i].orarioprevisto+`</td>
                  <td>`+ar[i].matricola+`</td>
                  <td><a href="confermaordine.php?toconfirm=`+ar[i].numordine+`&matricola=`+ar[i].matricola+`&orario=`+ar[i].orarioprevisto+`">Visualizza ordine</a></td>
                </tr>`);
          }
          setTimeout(function(){getPendingOrder();}, 5000);
        });
});
