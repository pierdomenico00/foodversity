<?php
  session_start();
  require_once "connect.php";
  $email = $_POST['email'];
  $password =  $_POST['password'];
  if(isset($_POST['remember'])){
    $remember = $_POST['remember'];
  }
  $query = mysqli_query($link, "SELECT * FROM cliente WHERE email = '$_POST[email]'");
  if($row = mysqli_fetch_array($query)) {
    if(password_verify($password, $row[4])) {
      if($email=="admin@foodversity.it"){
        $_SESSION["idcliente"] = "admin";
      }
      else {
        $_SESSION["idcliente"] = $row[0];
      }
      if(isset($remember)) {
        $cookie_name = "remember_me";
        $cookie_value = $_SESSION["idcliente"];
        setcookie($cookie_name, $cookie_value, time() + (86400), "/");
      }
      echo "Login riuscito";
      mysqli_close($link);
    } else {
      echo "Password errata";
      mysqli_close($link);
    }
  } else {
      echo "Utente non registrato";
      mysqli_close($link);
  }
?>
