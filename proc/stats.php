<?php
session_start();
require_once "connect.php";
$out=array();
$result=mysqli_query($link, "SELECT count(*) as clienti from cliente");
$data=mysqli_fetch_assoc($result);
array_push($out, $data['clienti']);
$result=mysqli_query($link, "SELECT count(*) as piatti from piatto");
$data=mysqli_fetch_assoc($result);
array_push($out, $data['piatti']);
$result=mysqli_query($link, "SELECT count(*) as ristoranti from fornitore");
$data=mysqli_fetch_assoc($result);
array_push($out, $data['ristoranti']);
$result = mysqli_query($link, "SELECT count(*) as numero, Fornitore.nome
                            FROM ordine
                            INNER JOIN fornitore ON Ordine.idfornitore=Fornitore.idfornitore
                            WHERE Ordine.stato='1'
                            GROUP BY nome
                            ORDER BY numero DESC
                            LIMIT 3");
$rows = array();
while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
}
array_push($out, $rows);
$result=mysqli_query($link, "SELECT count(*) as ordini from ordine where stato='1'");
$data=mysqli_fetch_assoc($result);
array_push($out, $data['ordini']);
$result=mysqli_query($link, "SELECT count(*) as segnalazioni from segnalazione where stato='0'");
$data=mysqli_fetch_assoc($result);
array_push($out, $data['segnalazioni']);
echo json_encode($out);
?>
