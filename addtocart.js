$(document).ready(function() {
  $('table').on('click', 'button.plus-button', function(){
    var nome = $(this).closest("tr").find('td:eq(0)').text();
    var ingredienti = $(this).closest("tr").find('td:eq(1)').text();
    var costo = $(this).closest("tr").find('td:eq(2)').text();
    var quantita = $(this).closest("tr").find('input').val();
    $.ajax({
      type: 'post',
      url: 'proc/addtocart.php',
      data: { nome : nome,
              ingredienti : ingredienti,
              costo : costo,
            quantita : quantita},
      success: function(result) {
        //Sostituire icona pulsante con sources/check-mark.png e rendere bottone non cliccabile
        $('tr.user-row td:contains('+nome+')').closest('.user-row').find('.plus-button-img').attr('src', 'sources/checked.png');
        $('tr.user-row td:contains('+nome+')').closest('.user-row').find('.plus-button').prop('disabled', true);
      }
    });
  });
});
