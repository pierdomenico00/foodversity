$.ajax({
 url: "./proc/getdishinfo.php",
 data: {nome : nome},
 dataType: 'json'
}).done(function(result) {
   var c = jQuery.parseJSON(JSON.stringify(result));
   $('#nome').val(c[0]['nome']);
   $('#ingredienti').val(c[0]['ingredienti']);
   $('#prezzo').val(c[0]['costo']);
 });
