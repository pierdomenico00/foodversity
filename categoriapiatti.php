<?php
  session_start();
  require_once "proc/connect.php";
  $_SESSION["categoria"] = $_GET["categoria"];
  if(!isset($_SESSION["idcliente"])){
    header("location:index.php");
  }
  $id = $_SESSION["id"];
  $idcat = $_GET["idcat"];
  $sth = mysqli_query($link, "SELECT nome, ingredienti, costo FROM piatto WHERE idfornitore = '$id' AND idcat = '$idcat'");
  $rows = array();
  while($r = mysqli_fetch_assoc($sth)) {
      $rows[] = $r;
  }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Ristorante</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        var ar = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($rows);?>));
        var index = ar.length;
        for (var i = 0; i < index; i++) {
          if (i%2==0) {
            $('#tabella').append(
              `<tr class="user-row">
                  <td style="font-weight:bold">`+ar[i].nome+`</td>
                  <td>`+ar[i].ingredienti+`</td>
                  <td>`+ar[i].costo+` €</td>
                  <td><input type="number" value="1" min="1" max="10" style="color:black; width:30px"></input></td>
                  <td><button type="button" class="plus-button plus-button-odd"><img src="sources/round-add-button.png" class="plus-button-img img-odd"/></button></td>
                </tr>`);
            } else if (i%2==1) {
              $('#tabella').append(
                `<tr class="user-row">
                    <td style="font-weight:bold">`+ar[i].nome+`</td>
                    <td>`+ar[i].ingredienti+`</td>
                    <td>`+ar[i].costo+` €</td>
                    <td><input type="number" value="1" min="1" max="10" style="color:black; width:30px"></input></td>
                    <td><button type="button" class="plus-button"><img src="sources/round-add-button.png" class="plus-button-img"/></button></td>
                  </tr>`);
            }
        }
      });
    </script>
    <script src="addtocart.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardcliente.php">FoodVersity</a>
          <a href="dashboardcliente.php">
            <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <div class="dropdown">
            <button type="button" class="btn btn-basic dropbtn button-noBorder">Account</button>
            <div class="dropdown-content">
              <a href="edituserinfo.php">Modifica</a>
              <a href="myorders.php">I miei Ordini</a>
            </div>
          </div>
          <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
          <a href="carrello.php"><img class="shopping-cart" src="./sources/shopping-cart.png"></a>
        </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 ristoranti-header">
          <h2 class="ristoranti-title"><?php echo $_SESSION["ristorante"];?></h2>
        </div>
      </div>
    </div>
    <div class="container" style="margin-bottom:30px">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
          <table style="border-collapse:collapse; width:100%">
            <thead>
              <tr style="background-color:white">
                <th style="width:30%; text-align:center"><?php echo $_SESSION["categoria"]; ?></th>
                <th style="width:45%; text-align:center">Ingredienti</th>
                <th style="width:15%; text-align:center">Prezzo</th>
                <th style="width:5%; text-align:center">Qt.</th>
                <th style="width:5%"></th>
              </tr>
            </thead>
            <tbody id="tabella">
            </tbody>
          </table>
        </div>
      </div>
    </div>
