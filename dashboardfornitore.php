<?php
session_start();
if (!isset($_SESSION["idsuppl"]))
 {
    header("location:loginfornitore.php");
 }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Dashboard fornitore</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="add_dish.js"></script>
    <script src="getpendingorder.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
          <a href="dashboardfornitore.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
      </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Gestione piatti</h4></div>
            <div class="panel-body">
              <form id="form" method="post">
                <div class="form-group">
                  <label for="categoria">Categoria:</label>
                  <select name="categoria" class="form-control">
                    <option value="antipasto">Antipasto</option>
                    <option value="primo">Primo piatto</option>
                    <option value="secondo">Secondo piatto</option>
                    <option value ="zuppa">Zuppa</option>
                    <option value="pizza">Pizza</option>
                    <option value="snack">Snack</option>
                    <option value="panino">Panino</option>
                    <option value="piadina">Piadina</option>
                    <option value="insalata">Insalata</option>
                    <option value="dolce">Dolce</option>
                    <option value="bibita">Bibita</option>
                    <option value="altro">Altro</option>
                  </select>
                  </div>
                <div class="form-group" style="margin-top:15px">
                  <label for="nome">Nome piatto:</label>
                  <input type="text" class="form-control" id="nome" placeholder="Inserisci nome piatto" name="nome" required>
                </div>
                <div class="form-group">
                  <label for="ingredienti">Ingredienti:</label>
                  <input type="text" class="form-control" id="ingredienti" placeholder="Inserisci lista ingredienti" name="ingredienti" required>
                </div>
                <div class="form-group">
                  <label for="prezzo">Prezzo piatto:</label>
                  <div class="input-group">
                    <span class="input-group-addon">€</span>
                    <input type="number" class="form-control" min="1" step="0.01" id="prezzo" placeholder="0,00" name="prezzo" required>
                  </div>
                </div>
                <button type="submit" class="btn btn-default">Aggiungi piatto</button>
              </form>
              <a href="visualizzazionepiatti.php" class="btn btn-default" style="margin-top:15px">Visualizza piatti</a>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Gestione ordini</h4></div>
            <div class="panel-body">
              <table class="resp-table resp-table-wrapped">
                <caption style="font-weight:bold; color:black">Ordini in attesa di conferma</caption>
                <thead>
                  <tr>
                    <th style="width:23%">ID Ordine</th>
                    <th style="width:21%">Luogo consegna</th>
                    <th style="width:17%; word-break:break-word">Orario consegna</th>
                    <th style="width:19%">Matricola</th>
                    <th style="width:20%"></th>
                  </tr>
                </thead>
                <tbody id="ordinipendenti">
                </tbody>
              </table>
              <a href="storicoordini.php" class="btn btn-default" style="margin-top:15px">Visualizza storico ordini</a><br>
              <a href="help.php" class="btn btn-default" style="margin-top:15px;">Segnala problemi</a>
            </div>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
  </body>
