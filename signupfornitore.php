<?php
session_start();
  if (isset($_SESSION["idsuppl"]) || isset($_SESSION["idcliente"]))
   {
      header("location:dashboardfornitore.php");
   }
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>FoodVersity supplier registration page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="suppl_signup.js"></script>
  <script src="img_upload.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">FoodVersity</a>
        <a href="index.php">
        <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
        </a>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Registrazione fornitore</h4></div>
          <div class="panel-body animated-form">
            <label class="form-label">Inserisci un'immagine che rappresenti il tuo ristorante:</label>
            <input class="form-label" id="image" type="file" name="image" required></br>
            <button id="upload" class="btn btn-default" value="Upload">Carica immagine</button><br>
            <label class="form-label">Consentiti solo .jpg con dimensione max 2 MB</label
            <br>
            <span style="color : white" id="statusphoto"></span><br><br>
            <form id="form" method="post">
              <div class="form-group">
                <label class="form-label" for="nome">Nome fornitore:</label>
                <input type="text" class="form-control" id="nome" placeholder="Inserisci il nome del tuo ristorante" name="nome" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="città">Città:</label>
                <input type="text" class="form-control" id="città" placeholder="Cesena" name="città" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="via">Via:</label>
                <input type="text" class="form-control" id="via" placeholder="dell'Università" name="via" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="ncivico">Numero civico:</label>
                <input type="text" class="form-control" id="ncivico" placeholder="1" name="ncivico" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="prova@prova.it" name="email" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="********" name="password" required>
              </div>
              <div class="form-group">
                <label class="form-label" for="confirm-password">Conferma password:</label>
                <input type="password" class="form-control" id="passwordconf" placeholder="********" name="passwordconf" required>
              </div>
              <span style="color : white" id="statusmsg"></span><br><br>
              <button type="submit" class="btn btn-default" value="Submit">Registrati</button>
            </form>
            </div>
          </div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
</body>
