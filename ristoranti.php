<?php
  session_start();
  $_SESSION["ristorante"] = $_GET["ristorante"];
  if(!isset($_SESSION["idcliente"])){
    header("location:index.php");
  }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Ristorante</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="respage.js"></script>
    <script src="getcat.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
        <nav class="navbar navbar-inverse">
          <div class="container">
            <div class="navbar-header">
              <a class="navbar-brand" href="dashboardcliente.php">FoodVersity</a>
              <a href="dashboardcliente.php">
              <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
              </a>
            </div>
          <ul class="nav navbar-nav navbar-right">
            <div class="dropdown">
              <button type="button" class="btn btn-basic dropbtn button-noBorder">Account</button>
              <div class="dropdown-content">
                <a href="edituserinfo.php">Modifica</a>
                <a href="myorders.php">I miei Ordini</a>
              </div>
            </div>
            <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
            <a href="carrello.php"><img class="shopping-cart" src="./sources/shopping-cart.png"></a>
          </ul>
          </div>
        </nav>
        <div class="container">
          <div class="col-md-3"></div>
          <div class="col-md-6 ristoranti-header">
            <h2 class="ristoranti-title"><?php echo $_SESSION["ristorante"] ?></h2>
          </div>
        </div>
    <div id="categorie" class="container">
    </div>
</body>
