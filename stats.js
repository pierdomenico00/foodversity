$(document).ready(function getStats() {
  $.ajax({
        url: "proc/stats.php",
        type: "get",
  }).done(function(result) {
    var ar = jQuery.parseJSON(result);
    $("#usernumber").html(ar[0]);
    $("#dishnumber").html(ar[1]);
    $("#supplnumber").html(ar[2]);
    $("#ordernumber").html(ar[4]);
    $("#ticketnumber").html(ar[5]);
    var ctx = document.getElementById("myChartBarOrder");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [ar[3][0]["nome"], ar[3][1]["nome"], ar[3][2]["nome"]],
            datasets: [{
                label: 'Top 3 per numero di ordini',
                data: [ar[3][0]["numero"], ar[3][1]["numero"], ar[3][2]["numero"]],
                backgroundColor: [
                    'rgb(213, 0, 40)',
                    'rgb(213, 0, 40)',
                    'rgb(213, 0, 40)'
                ],
                borderColor: [
                    'rgb(160, 17, 27)',
                    'rgb(160, 17, 27)',
                    'rgb(160, 17, 27)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            animation: false,
            scales: {
                xAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: 1
                    }
                }]
            }
        }
    });
      setTimeout(function(){getStats();}, 5000);
    });
  });
