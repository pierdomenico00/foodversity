<?php
  session_start();
  $_SESSION["toconfirm"] = $_GET["toconfirm"];
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Conferma ordine</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="confirmorder.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
          <a href="dashboardcliente.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-basic button-noBorder" onclick="location='edituserinfo.php'">Account</button>
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
      </ul>
      </div>
    </nav>
    <div id="prodotto" class="container">
    </div>
    <div class="clear"></div>
    <div class="container footer">
      <div class="jumbotron jumbrotron-specific">
          <div class="col-md-3">
            <p class="user-information">Orario : <?php echo $_GET["orario"]?></p>
          </div>
          <div class="col-md-3">
            <p class="user-information">Matricola : <?php echo $_GET["matricola"]?></p>
          </div>
          <div class="col-md-3">
            <p id="totale" class="user-information"></p>
          </div>
        <div class="row">
          <div class="col-md-12">
            <button onclick="location.href = 'proc/acceptorder.php';" id="accetta" type="button" class="btn myButtonOrder pull-right">Accetta</button>
            <button onclick="location.href = 'proc/rejectorder.php';" id="rifiuta" type="button" class="btn myButtonOrder pull-right">Rifiuta</button>
          </div>
        </div>
      </div>
    </div>
  </body>
