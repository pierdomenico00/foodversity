<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Carrello</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
      var c = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($_SESSION["cart"]);?>));
      var index = c.length;
      var total = 0;
      for (var i = 0; i < index; i++) {
        var last = c.pop();
        $('#prodotto').append(`<div id="dish" class="jumbotron">
                  <div class="row">
                    <div class="col-md-11">
                      <h2 class="dish-name">`+last[0]+`</h2>
                    </div>
                      <div class="col-md-1 text-right">
                          <button type="button" class="btn delate-button">Elimina</button>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-7">
                      <p>`+last[1]+`</p>
                    </div>
                    <div class="col-md-3">
                        Quantità:<span id="quantita">`+last[2]+`</span>
                    </div>
                    <div class="col-md-2">
                      <div class="price"><span id="price" class="input-group-addon">`+last[3]+`</span></div>
                    </div>
                  </div>
              </div>`);
              total = total + Number(last[3].substr(0,last[3].indexOf(' '))*last[2])
            }
            total = parseFloat(total).toFixed(2);
            $('#totale').text('Totale: '+total+' €');
      });
    </script>
    <script src="removefromcart.js"></script>
    <script src="placeorder.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardcliente.php">FoodVersity</a>
          <a href="dashboardcliente.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <div class="dropdown">
          <button type="button" class="btn btn-basic dropbtn button-noBorder">Account</button>
          <div class="dropdown-content">
            <a href="edituserinfo.php">Modifica</a>
            <a href="myorders.php">I miei Ordini</a>
          </div>
        </div>
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
        <a href="carrello.php"><img class="shopping-cart" src="./sources/shopping-cart.png"></a>
      </ul>
      </div>
    </nav>
    <div id="prodotto" class="container">
    </div>
    <div class="clear"></div>
    <div class="container footer">
      <div class="jumbotron jumbrotron-specific">
        <div class="row">
          <div class="col-md-4">
            <form id="form" method="post">
              <div class="form-group">
                <label for="categoria" style="color: white;">Luogo di consegna:</label>
                <select name="categoria" class="form-control">
                  <option>Punto ristoro universitario</option>
                </select>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <form id="form" method="post">
              <div class="form-group">
                <label for="categoria" style="color: white;">Orario di consegna:</label>
                <select id="orario" name="categoria" class="form-control">
                  <option>8:00</option>
                  <option>8:30</option>
                  <option>9:00</option>
                  <option>9:30</option>
                  <option>10:00</option>
                  <option>10:30</option>
                  <option>11:00</option>
                  <option>11:30</option>
                  <option>12:00</option>
                  <option>12:30</option>
                  <option>13:00</option>
                  <option>13:30</option>
                  <option>14:00</option>
                  <option>14:30</option>
                  <option>15:00</option>
                  <option>15:30</option>
                  <option>16:00</option>
                  <option>16:30</option>
                  <option>17:00</option>
                  <option>17:30</option>
                  <option>18:00</option>
                  <option>18:30</option>
                  <option>19:00</option>
                  <option>19:30</option>
                </select>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <p id="totale" class="user-information"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn myButtonOrder pull-right">Ordina e paga</button>
          </div>
        </div>
      </div>
    </div>
  </body>
