$(document).ready(function() {
  $('div.jumbotron').on('click', 'button.myButtonOrder', function(){
    var orario = $("#orario :selected").text();
    $.ajax({
      type: 'post',
      url: 'proc/placeorder.php',
      data: {orario: orario},
      success: function() {
        alert("Ordine inserito con successo. Riceverai un'email per la conferma dell'ordine.");
        window.location.href = "dashboardcliente.php";
      }
    });
  });
});
