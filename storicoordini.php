<?php
session_start();
require_once "proc/connect.php";
if(!isset($_SESSION["idsuppl"])){
  header("location:index.php");
}
$id=$_SESSION["idsuppl"];
$sth = mysqli_query($link, "SELECT numordine, orarioprevisto, matricola FROM ordine WHERE idfornitore = '$id' AND stato = '1'");
$rows = array();
while($r = mysqli_fetch_assoc($sth)) {
    $rows[] = $r;
}
 ?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Storico ordini</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="suppl_signup.js"></script>
  <script src="img_upload.js"></script>
  <script>
    $(document).ready(function() {
      var ar = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($rows);?>));
      var index = ar.length;
      for (var i = 0; i < index; i++) {
          $('#tabella').append(
            `<tr>
              <td>`+ar[i].numordine+`</td>
              <td>`+ar[i].orarioprevisto+`</td>
              <td>`+ar[i].matricola+`</td>
              <td style="text-align:center"><a href="ordine.php?numero=`+ar[i].numordine+`" style="text-align:center">Dettagli</a></td>
              </tr>`);
      }
    });
  </script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
        <a href="dashboardfornitore.php">
        <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
        </a>
      </div>
    <ul class="nav navbar-nav navbar-right">
      <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
    </ul>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Storico ordini</h4></div>
          <div class="panel-body">
            <table class="resp-table resp-table-wrapped">
              <caption style="font-weight:bold; color:black">Ordini confermati</caption>
              <thead>
                <tr>
                  <th>Num. ordine</th>
                  <th>Orario consegna</th>
                  <th>Matricola</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="tabella">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</body>
