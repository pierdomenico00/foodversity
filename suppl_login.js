$(function () {
  $('form').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: 'proc/suppliercheck.php',
      data: $('form').serialize(),
      success: function(result) {
          if(result == "Login riuscito") {
            window.location.href='dashboardfornitore.php';
          } else {
            document.getElementById("statusmsg").innerHTML = result;
          }
      }
    });
  });
});
