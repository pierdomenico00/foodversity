<?php
  session_start();
  if(!isset($_SESSION["idsuppl"])){
    header("location:index.php");
  }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Ristorante</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>var nome = "<?php echo $_GET["nome"];?>"</script>
    <script src="getdishinfo.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardfornitore.php">FoodVersity</a>
          <a href="dashboardfornitore.php">
            <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
        </ul>
      </div>
    </nav>
    <div class="container">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Modifica piatto</h4></div>
          <div class="panel-body">
            <form id="form" method="post" action="proc/modifydish.php">
              <div class="form-group" style="margin-top:15px">
                <label for="nome">Nome piatto:</label>
                <input type="text" class="form-control" id="nome" placeholder="Inserisci nome piatto" name="nome" required>
              </div>
              <div class="form-group">
                <label for="ingredienti">Ingredienti:</label>
                <input type="text" class="form-control" id="ingredienti" placeholder="Inserisci lista ingredienti" name="ingredienti" required>
              </div>
              <div class="form-group">
                <label for="prezzo">Prezzo piatto:</label>
                <div class="input-group">
                  <span class="input-group-addon">€</span>
                  <input type="number" class="form-control" min="1" step="0.01" id="prezzo" placeholder="0,00" name="prezzo" required>
                </div>
              </div>
              <button type="submit" class="btn btn-default">Modifica piatto</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
