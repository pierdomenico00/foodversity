<?php
session_start();
require_once "proc/connect.php";
if (!isset($_SESSION["idcliente"]) && !isset($_SESSION["idsuppl"]))
 {
    header("location:index.php");
 }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Invia segnalazione</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="sendticket.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">FoodVersity</a>
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
      </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Invia segnalazione</h4></div>
            <div class="panel-body animated-form">
                <div class="form-group">
                  ​<textarea id="txtArea" maxlength="400" rows="10" cols="70" required></textarea>
                  <label id="max" style="color: white">Max. 400 caratteri</label><br><br>
                  <label id="status" style="color: white"></label>
                </div>
                <button onclick="sendTicket(); return false;" class="btn btn-default">Invia</button>
            </div>
          </div>
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
  </body>
</html>
