<?php
  session_start();
  if(!isset($_SESSION["idcliente"])){
    header("location:index.php");
  }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Modifica informazioni</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="getinfo.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboardcliente.php">FoodVersity</a>
          <a href="dashboardcliente.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
      </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Modifica le tue informazioni</h4></div>
            <div class="panel-body">
              <form action="proc/updateinfo.php" method="post">
                <div class="form-group">
                  <label for="nome">Nome:</label>
                  <input type="text" class="form-control" id="nome" placeholder="" name="nome">
                </div>
                <div class="form-group">
                  <label for="cognome">Cognome:</label>
                  <input type="text" class="form-control" id="cognome" placeholder="" name="cognome">
                </div>
                <div class="form-group">
                  <label for="email">Email:</label>
                  <input type="email" class="form-control" id="email" placeholder="" name="email">
                </div>
                <button type="submit" class="btn btn-default">Aggiorna informazioni</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Modifica la tua password</h4></div>
            <div class="panel-body">
              <form action="proc/updatepass.php" method="post">
                <div class="form-group">
                  <label for="oldpassw">Vecchia password:</label>
                  <input type="password" class="form-control" id="oldpassw" placeholder="********" name="oldpassw" required>
                </div>
                <div class="form-group">
                  <label for="newpassw">Nuova password:</label>
                  <input type="password" class="form-control" id="newpassw" pattern=".{8,}" required title="minimo 8 caratteri" placeholder="********" name="newpassw" required>
                </div>
                <div class="form-group">
                  <label for="confpassw">Conferma password:</label>
                  <input type="password" class="form-control" id="confpassw" pattern=".{8,}" required title="minimo 8 caratteri" placeholder="********" name="confpassw" required>
                </div>
                <button type="submit" class="btn btn-default">Aggiorna password</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-1">
        </div>
      </div>
    </div>
    <a href="help.php" class="btn btn-default bottom-left-btn" style="margin-right:10px">Segnala problemi</a>
</body>
