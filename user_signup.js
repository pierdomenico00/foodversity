$(function () {
  $('form').on('submit', function (e) {
    e.preventDefault();
    if ((document.getElementById("password").value.length) < 8) {
      document.getElementById("statusmsg").innerHTML = "La password dovrebbe contenere almeno 8 caratteri.";
      document.getElementById("password").value = '';
      document.getElementById("passwordconf").value = '';
      return false;
    } else if (document.getElementById("password").value != document.getElementById("passwordconf").value) {
      document.getElementById("statusmsg").innerHTML = "Le password non corrispondono.";
      document.getElementById("password").value = '';
      document.getElementById("passwordconf").value = '';
      return false;
      }
    $.ajax({
      type: 'post',
      url: 'proc/register.php',
      data: $('form').serialize(),
      success: function(result) {
          if(result == "Registrazione completata") {
            alert("Registrazione completata! Verrai reindirizzato alla pagina di login.");
            window.location.href='login.php';
          } else {
            document.getElementById("statusmsg").innerHTML = result;
          }
      }
    });
  });
});
