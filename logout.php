<?php
  session_start();
  session_destroy();
  if(isset($_COOKIE['remember_me'])){
      setcookie('remember_me', null, -1, '/');
  }
  if(isset($_COOKIE['remember_suppl'])){
      setcookie('remember_suppl', null, -1, '/');
  }
  printf("Logging out...");
  header("location:index.php");
 ?>
