<?php
session_start();
require_once "proc/connect.php";
if ($_SESSION["idcliente"]!="admin")
 {
    header("location:login.php");
 }
 $sth = mysqli_query($link, "SELECT nome FROM fornitore");
 $rows = array();
 while($r = mysqli_fetch_assoc($sth)) {
     $rows[] = $r;
 }
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Gestione amministratore</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script src="stats.js"></script>
  <script>
    $(document).ready(function() {
      var ar = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($rows);?>));
      var index = ar.length;
      for (var i = 0; i < index; i++) {
          $('#tabella').append(
            `<tr>
              <td>`+ar[i].nome+`</td>
              <td style="text-align:center"><button type="button" class="btn delate-button" onclick="location.href = 'proc/removesupplier.php?nome=`+ar[i].nome+`';">Rimuovi</button></td>
              </tr>`);
      }
    });
  </script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="admin.php">FoodVersity</a>
        <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
        </a>
      </div>
    <ul class="nav navbar-nav navbar-right">
      <button type="button" class="btn btn-default button-squared" onclick="location='logout.php'">Log out</button>
    </ul>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="jumbotron admin-info-container">
          <h3>Utenti</h3>
          <p id="usernumber"class="admin-info"></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="jumbotron admin-info-container">
          <h3>Piatti</h3>
          <p id="dishnumber" class="admin-info"></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="jumbotron admin-info-container">
          <h3>Ristoranti</h3>
          <p id="supplnumber" class="admin-info"></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="jumbotron admin-info-container">
          <canvas id="myChartBarOrder" height="200px"></canvas>
        </div>
      </div>
      <div class="col-md-6">
        <div class="jumbotron admin-info-container">
          <h3>Ordini totali</h3>
          <p id="ordernumber" class="admin-info"></p>
        </div>
        <div class="jumbotron admin-info-container">
          <h3>Segnalazioni pendenti</h3>
          <p id="ticketnumber" class="admin-info"></p><br>
           <a href="segnalazioni.php">Visualizza segnalazioni</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Disattiva fornitore</h4></div>
          <div class="panel-body">
            <table class="resp-table resp-table-wrapped">
              <caption style="font-weight:bold; color:black">Lista fornitori</caption>
              <thead>
                <tr>
                  <th>Ristorante</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="tabella">
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</body>
</html>
