<?php
session_start();
if (isset($_SESSION["idsuppl"]))
 {
    header("location:dashboardfornitore.php");
 }
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <title>FoodVersity supplier login page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet search" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="suppl_login.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">FoodVersity</a>
          <a href="index.php">
          <img alt="Brand" class="img-circle img-responsive" src="./sources/cappelloQuadrato.png">
          </a>
        </div>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading"><h4>Login fornitore</h4></div>
            <div class="panel-body animated-form">
              <form name="form" method="post">
                <div class="form-group">
                  <label class="form-label" for="email">Email:</label>
                  <input type="email" class="form-control" id="email" placeholder="Inserisci email" name="email" required>
                </div>
                <div class="form-group">
                  <label class="form-label" for="password">Password:</label>
                  <input type="password" class="form-control" id="password" placeholder="Inserisci password" name="password" required>
                </div>
                <span style="color : white" id="statusmsg"></span>
                <div class="checkbox">
                  <label style="color:white"><input id="remember_suppl" type="checkbox" name="remember_suppl"> Ricordami </label>
                </div>
                <button type="submit" class="btn btn-default" value="Submit">Invia</button>
                <br><br>
                <p style="font-weight:bold; color:white">Nuovo fornitore? <a style="color:white; text-decoration:underline" href="signupfornitore.php">Registrati</a> ora!</p>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
  </body>
